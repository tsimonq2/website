<!--
.. title: Contact Us | Altispeed Technologies
.. slug: contact
.. date: 2019-01-01 06:00:00 UTC
.. tags: Altispeed,Technologies,Tech,Open,Source,OpenSource,Solutions,Contact
.. link:
.. description: This is how to contact the team at Altispeed Technologies.
.. type: text
.. hidetitle: true
-->


## Want to talk with someone?

Get help by phone, chat, or email, set up an on-site service call, or make a remote support request.

<section>
    <div class="contactleft">
        <b>Address:</b> <br />
        Altispeed Technologies <br />
        1191 S. Columbia Rd <br />
        Grand Forks, ND 58201 <br />
        <b>Phone number:</b> <a href="tel:17014022300">+1 (701) 402-2300</a> <br />
        <b>Email address:</b> <a href="mailto:support@altispeed.com">support@altispeed.com</a> <br />
        <b>Looking for emergency support? Dial <a href="tel:17014022300">+1 (701) 402-2300</a> and press 5.</b><br /><br />
        <b>Operating hours:</b> Monday through Friday, 10 AM to 6 PM Central Time <br />
        We aim to return all emails and calls within one business day.
    </div>
    <div class="contactright">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d472.7645841785696!2d-97.06635523193478!3d47.91006464650086!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x52c68123a4d804dd%3A0xf7a973322d9179bd!2s1191+S+Columbia+Rd%2C+Grand+Forks%2C+ND+58201!5e0!3m2!1sen!2sus!4v1548095753926" frameborder="0" style="border:0" allowfullscreen height="300em" width="400em"></iframe>
    </div>
</section>





<br />
<br />
<br />

## Online Customer Support

There's no waiting on hold to chat online!
[Launch Chat!](https://tawk.to/chat/5ca62fa86bba4605280149df/default)




<br />
<br />
<br />

## Contact Form

# <div id="wufoo-zass29j1x11hi5">
Fill out my <a href="https://altispeed.wufoo.com/forms/zass29j1x11hi5">online form</a>.
</div>
<div id="wuf-adv" style="font-family:inherit;font-size: small;color:#a7a7a7;text-align:center;display:block;">Online contact and registration forms from <a href="http://www.wufoo.com">Wufoo</a>.</div>
<script type="text/javascript">var zass29j1x11hi5;(function(d, t) {
var s = d.createElement(t), options = {
'userName':'altispeed',
'formHash':'zass29j1x11hi5',
'autoResize':true,
'height':'595',
'async':true,
'host':'wufoo.com',
'header':'show',
'ssl':true};
s.src = ('https:' == d.location.protocol ? 'https://' : 'http://') + 'secure.wufoo.com/scripts/embed/form.js';
s.onload = s.onreadystatechange = function() {
var rs = this.readyState; if (rs) if (rs != 'complete') if (rs != 'loaded') return;
try { zass29j1x11hi5 = new WufooForm();zass29j1x11hi5.initialize(options);zass29j1x11hi5.display(); } catch (e) {}};
var scr = d.getElementsByTagName(t)[0], par = scr.parentNode; par.insertBefore(s, scr);
})(document, 'script');</script>

<br />
<br />
<br />
<br />
<br />
<br />


