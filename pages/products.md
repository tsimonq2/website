<!--
.. title: Products | Altispeed Technologies
.. slug: products
.. date: 2019-01-01 06:00:00 UTC
.. tags: Altispeed,Technologies,Tech,Open,Source,OpenSource,Solutions,Services,Provided
.. link:
.. description: These are the products sold by Altispeed Technologies.
.. type: text
.. hidetitle: true
-->

##  Ubiquiti Networks Unifi 802.11ac Dual-Radio PRO Managed Access Point
### $249.99

<div align="left">
 <img src="/images/products/uapacpro.jpg" alt="uap-ac-pro">
</div>

Deploy the UniFi AC Pro AP indoors or outdoors, in wireless networks requiring maximum performance. Sporting a weatherproof design, the UniFi AC Pro AP features simultaneous, dual-band, 3x3 MIMO technology and convenient 802.3af PoE/802.3at PoE+ compatibility. 

+ 3 Dual-Band Antennas, 3 dBi each
+ Max. Power Consumption: 9W
+ Networking Interface: 2 10/100/1000 Ethernet Ports
+ Features auto-sensing 802.3af/802.3at PoE support and can be powered by any of the following: Ubiquiti Networks
+ UniFiwitch 802.3af/802.3at PoE+ compliant switch Ubiquiti Networks Gigabit PoE Adapter (48V, 0.5A)
+ Wi-Fi Standards: 802.11 a/b/g/n/ac 

<script async src="https://static-na.payments-amazon.com/OffAmazonPayments/us/js/Widgets.js"></script>
<div
    data-ap-widget-type="expressPaymentButton"
    data-ap-signature="BVQlHpyXZ1AkreXb5AK1aV3O12kcsXM6kjYzDhjoIHA%3D"
    data-ap-seller-id="ATMO94DSFEYU2"
    data-ap-access-key="AKIAIUWX4HNMRUAS276A"
    data-ap-lwa-client-id="amzn1.application-oa2-client.d90f040e03964089be125218fd7a854d"
    data-ap-return-url="https://www.altispeed.com"

        data-ap-cancel-return-url="https://www.altispeed.com"

    data-ap-currency-code="USD"
    data-ap-amount="249.99"
    data-ap-note=""
    data-ap-shipping-address-required="true"
    data-ap-payment-action="AuthorizeAndCapture"
>
</div>






<br />
<br />
<br />
