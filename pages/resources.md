<!--
.. title: Resources | Altispeed Technologies
.. slug: resources
.. date: 2019-01-01 06:00:00 UTC
.. tags: Altispeed,Technologies,Tech,Open,Source,OpenSource,Solutions,Resources
.. link:
.. description: Altispeed Technologies' resources.
.. type: text
.. hidetitle: true
-->

# Popular Client Resources


<br/>

[Check Dell Warranty Status](https://www.dell.com/support/home/yu/en/yubsdt1?app=warranty)
> Identify your product to see your warranty status and coverage options. Learn about Dell warranty registration, transfer, renewal, and expired-warranty services.


[Dell Drivers](http://www.dell.com/support/drivers/us/en/19/DriversHome/ShowProductSelector)
> Download drivers for your Dell PC. Identify your product to get the latest available updates.


[Remote Support](http://support.altspd.com/customer)
> Use only when directed by a customer support representative. This software will allow you to connect with an Altispeed support technician to diagnose and correct issues remotely.


[Enroll Client Machine](http://support.altspd.com/access)
> This software is for use by clients with a support agreement in place. This will enroll your computer into our system for ongoing remote support.

[HP Printer Drivers](https://support.hp.com/us-en/drivers/)
> Download the drivers required for your HP Printer. 

[Knowledge Base](http://portal.altispeed.com)
> We have the help you need — all in one place. Get the tips, tricks, and step by step guides for free on demand with our comprehensive Knowledge Base!

[HP M402N Windows Driver](https://ftp.hp.com/pub/softlib/software13/printers/IPG/M402/HP_LJ_Pro_M402-M403_n-m-dn-dne_PCL6_Print_Driver_no_Installer_16147.exe)
> Altispeed recommends the HP M402n for most applications. Download the driver to get your printer working with Windows 10.


<a  href="#" onclick="var left = (screen.width/2) - 150; var top = (screen.height/2) - 60; window.open('http://www.newpasswordgenerator.com/generate.php?type=1&length=10&digits=2','passwindow','width=300,height=120,resizable=yes,scrollbars=no,location=yes,left='+left+',top='+top);" >Generate a New Password</a>

> Treat your passwords like underwear, the longer the better, don't share them, and most importantly change them often!



<br />
<br />
<br />
<br />
<br />
<br />



## Make an Online Payment to Your Account

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script type="text/javascript">

Stripe.setPublishableKey('pk_live_F8DH33wXwPwiX4dMZZv4EASC');

function onSubmitDo () {

  Stripe.card.createToken( document.getElementById('payment-form'), myStripeResponseHandler );

  return false;

};
function myStripeResponseHandler ( status, response ) {

  console.log( status );
  console.log( response );

  if ( response.error ) {
    document.getElementById('payment-error').innerHTML = response.error.message;
  } else {
    var tokenInput = document.createElement("input");
    tokenInput.type = "hidden";
    tokenInput.name = "stripeToken";
    tokenInput.value = response.id;
    var paymentForm = document.getElementById('payment-form');
    paymentForm.appendChild(tokenInput);
    paymentForm.submit();
  }

};
</script>
<div id="payment-error"></div>
<form action="https://payments.altispeed.com/pay.php" method="post" onsubmit="return onSubmitDo()" name="payment-form" id="payment-form">

<div class="left_name"><strong>Amount :<span>*</span> </strong></div>
<div class="right_field"><input name="amount" data-stripe="amount" id="amount" type="text"></div>

<div class="left_name"><strong>First Name :<span>*</span> </strong></div>
<div class="right_field"><input name="fname" data-stripe="fname" id="fname" type="text"></div>

<div class="left_name"><strong>Last Name :<span>*</span> </strong></div>
<div class="right_field"><input name="lname" data-stripe="lname" id="lname" type="text"></div>

<div class="left_name"><strong>Email :<span>*</span></strong></div>
<div class="right_field"><input name="email" data-stripe="email" id="email" type="text"></div>

<div class="left_name"><strong>Invoice Number :</strong></div>
<div class="right_field"><input name="invoice_number" data-stripe="invoice_number" id="invoice_number" type="text"></div>

<div class="left_name"><strong>Card Number :<span>*</span></strong></div>
<div class="right_field"><input name="number" data-stripe="number" id="number" type="text"></div>

<div class="left_name"><strong>Expiration Date (MM/YY) :<span>*</span> </strong></div>
<div class="right_field"><input name="exp-month" id="exp-month" data-stripe="exp-month" type="text"> / <input name="exp_year" id="exp_year" data-stripe="exp_year" type="text"></div>

<div class="left_name"><strong>CVC :<span>*</span></strong></div>
<div class="right_field"><input name="cvc" data-stripe="cvc" id="cvc" type="text"></div>

<div class="left_name">&nbsp; </div>
<div class="right_field"><input type="submit" id="send" value="Pay" name="book_submit"></div>


<br/>
<br/>
<br/>
<br/>
<br/>
<br/>


<Center>

<a href="http://en.dnstools.ch/show-my-ip.html" target="_blank" title="Show my IP"><img  width="200" height="100" src="http://en.dnstools.ch/out/3.gif" alt="Show my IP" style="border:0"/></a>

</Center>