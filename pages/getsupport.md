<!--
.. title: Get Support | Altispeed Technologies
.. slug: getsupport
.. date: 2019-01-01 06:00:00 UTC
.. tags: Altispeed,Technologies,Tech,Open,Source,OpenSource,Solutions,Package,Deals,PackageDeals
.. link:
.. description: Get Support provided by Altispeed Technologies.
.. type: text
.. hidetitle: true
-->

## Let's get that fixed

If your network or computer needs repair, you can [schedule online](http://portal.altispeed.com/open.php) a remote support specialist to address the issue. For more complex issues you can request an on-site support technician to come to your business and work through the problem with you face to face.

Our call center is open 24/7 at 1-866-280-1433! Want help faster? Check out our chat widget and start a chat with Altispeed right now!

<br />
<br />
<br />


## Remote Support Portal

If you received instructions from a Customer Service Representative to begin a remote support session please [download the client here](http://support.altspd.com/customer).

<br />
<br />
<br />


## Get Instant Answers

We have the help you need — all in one place. Get the tips, tricks, and step by step guides for free on demand with our comprehensive [Knowledge Base](http://portal.altispeed.com/kb/index.php)!



<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5ca62fa86bba4605280149df/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
