<!--
.. title: About Us | Altispeed Technologies
.. slug: about
.. date: 2019-01-01 06:00:00 UTC
.. tags: Altispeed,Technologies,Tech,Open,Source,OpenSource,Solutions,About
.. link:
.. description: About Altispeed Technologies.
.. type: text
.. hidetitle: true
-->

## What Makes us Different?

<br/>

Altispeed Technologies was founded in 2009 and has been setting itself apart ever since by utilizing open source technology to provide creative solutions to clients without sacrificing quality. We’ve worked to earn our client's business, not just in Grand Forks but across the United States and around the world.

<br/>

Our goal is to develop partnerships with your business to help you grow without the daily hassle of IT maintenance and support. Every product we carry is used either in our shop or in our homes. We stand behind our products and our service.

<br/>
<br/>
<br/>
<br/>