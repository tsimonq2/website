<!--
.. title: Altispeed Technologies
.. slug: index
.. date: 2019-01-01 06:00:00 UTC
.. tags: Altispeed,Technologies,Tech,Open,Source,OpenSource,Solutions,Homepage
.. link:
.. description:t
.. type: text
.. hidetitle: true
-->

<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<div align="center">
    <b><h1>Altispeed Technologies</h1></b><br />
    <!--
    NOTE FOR ADDING IMAGES: For visual purposes, all images should be the same height
    -->
    <div id="banner-fade">
        <!-- start basic jQuery slider -->
        <ul class="bjqs">
            <li><a class="image-reference" href="/images/kiosk.jpg"><img src="/images/kiosk.jpg" title="Altispeed's Kiosk Machines"></a></li>
            <li><a class="image-reference" href="/images/network1.jpg"><img src="/images/network1.jpg" title="We provide networking for businesses of all sizes"></a></li>
            <li><a class="image-reference" href="/images/server1.jpg"><img src="/images/server1.jpg" title="We provide server support for your business"></a></li>
            <li><a class="image-reference" href="/images/camera-full.jpg"><img src="/images/camera-full.jpg" title="Altispeed's Camera Install"></a></li>
        </ul>
        <!-- end basic jQuery slider -->
    </div>
</div>
<script src="/assets/js/bjqs-1.3.min.js"></script>
<script>
    jQuery(document).ready(function($) {
    $('#banner-fade').bjqs({
        width : 852,
        height : 480,
        animspeed : 2500,
        responsive : true,
        usecaptions : false
    });
});
</script>

<br />
<br />
<br />



## Trusted enterprise IT support for The Red River Valley!

 - Save time and Money – Let us manage your IT infrastructure so you can focus on what you do best – your business!
 - Control your Costs - Our flat rate IT and managed services means you’ll know before the month begins what your IT costs are.
 - People who care – You’re not just a “client” to us. We step into your business and walk along side you and you concerns. We treat every problem as if it were our own.

Want to learn more? Give us a call today at 1-866-280-1433 and let us show you what we can do for your business.

<br />
<br />
<br />

## Support Portal

If you received instructions from a Customer Service Representative to begin a remote support session please download the client here.

<br />

 <div align="center">
<a href="http://support.altspd.com/customer"><img src="/images/remote-support.png" width="200" height="104" title="RemoteSupport" alt="RemoteSupport"></a>
</div>



<br />
<br />
<br />


## Follow us on Twitter

 Stay up to date with Altispeed by following us on Twitter!

<br />
<br />
<br />


<center>
<a class="twitter-timeline" data-lang="en" data-width="500" data-height="400" data-dnt="true" data-theme="dark" data-link-color="#fe7802" data-chrome="noheader nofooter noborders transparent" href="https://twitter.com/altispeed?ref_src=twsrc%5Etfw">Tweets by Altispeed</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</center>


<br />
<br />
<br />
<br />
<br />
<br />



