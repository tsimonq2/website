cd /srv/altispeed.com
git pull https://gitlab.com/altispeed/website.git
scl enable rh-python36 bash
virtualenv nikola
cd nikola
source bin/activate
cd ..
nikola build
exit
